// src/js/components/List.js
import React from "react";
import { connect } from "react-redux";
const mapStateToProps = state => {
  return { articles: state.articles };
};
const ConnectedList = ({ articles }) => (
  <div>
    {articles.map(el => (
      <div className="d-flex align-items-start">
        <p key={el.id}>{el.title}</p>
        <button
          className="btn btn-danger btn-sm"
          onClick={() =>
            this.props.dispatch({ type: "DELETE_POST", id: this.props.post.id })
          }
        >
          Delete
        </button>
      </div>
    ))}
  </div>
);
const List = connect(mapStateToProps)(ConnectedList);
export default List;
